QT += widgets
requires(qtConfig(listview))

SOURCES += \
    adddialog.cpp \
    customsortfiltermodel.cpp \
    main.cpp \
    mainwindow.cpp \
    newprofiletab.cpp \
    profilewidget.cpp \
    tablemodel.cpp

HEADERS += \
    adddialog.h \
    customsortfiltermodel.h \
    mainwindow.h \
    newprofiletab.h \
    profilewidget.h \
    tablemodel.h

FORMS += \
    mainwindow.ui

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/itemviews/addressbook
INSTALLS += target
