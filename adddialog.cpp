#include "adddialog.h"
#include <QTextStream>
#include <QtWidgets>

/* This creates the window that pops up when the user chooses to add a Persona. */
AddDialog::AddDialog(QWidget *parent)
    : QDialog(parent),
      nameText     (new QLineEdit), arcanaText       (new QLineEdit), personalityText (new QLineEdit),
      traitText    (new QLineEdit), defaultTraitText (new QLineEdit), levelEdit       (new QSpinBox ),
      strengthEdit (new QSpinBox ), magicEdit        (new QSpinBox ), enduranceEdit   (new QSpinBox ),
      agilityEdit  (new QSpinBox ), luckEdit         (new QSpinBox ), isCurrentCheck  (new QCheckBox)
{
    // Define the labels for the window items.
    auto nameLabel         = new QLabel(tr("Name"));             auto arcanaLabel         = new QLabel(tr("Arcana"));
    auto personalityLabel  = new QLabel(tr("Personality"));      auto traitLabel          = new QLabel(tr("Trait"));
    auto defaultTraitLabel = new QLabel(tr("Default Trait"));    auto levelBoxLabel       = new QLabel(tr("Level"));
    auto strengthBoxLabel  = new QLabel(tr("Strength"));         auto magicBoxLabel       = new QLabel(tr("Magic"));
    auto enduranceBoxLabel = new QLabel(tr("Agility"));          auto agilityBoxLabel     = new QLabel(tr("Endurance"));
    auto luckBoxLabel      = new QLabel(tr("Luck"));             auto isCurrentCheckLabel = new QLabel(tr("Is in current party?"));
    auto okButton          = new QPushButton(tr("ARE YOU OK?")); auto cancelButton        = new QPushButton(tr("Cancel"));
    auto gLayout           = new QGridLayout;

    // Populate the window.
    gLayout->setColumnStretch(1, 2);  //column, stretch
    gLayout->addWidget(nameLabel,           0, 0);
    gLayout->addWidget(nameText,            0, 1);

    gLayout->addWidget(arcanaLabel,         1, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(arcanaText,          1, 1, Qt::AlignLeft);

    gLayout->addWidget(personalityLabel,    2, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(personalityText,     2, 1, Qt::AlignLeft);

    gLayout->addWidget(traitLabel,          3, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(traitText,           3, 1, Qt::AlignLeft);

    gLayout->addWidget(defaultTraitLabel,   4, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(defaultTraitText,    4, 1, Qt::AlignLeft);

    gLayout->addWidget(levelBoxLabel,       5, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(levelEdit,           5, 1, Qt::AlignLeft);

    gLayout->addWidget(strengthBoxLabel,    6, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(strengthEdit,        6, 1, Qt::AlignLeft);

    gLayout->addWidget(magicBoxLabel,       7, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(magicEdit,           7, 1, Qt::AlignLeft);

    gLayout->addWidget(enduranceBoxLabel,   8, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(enduranceEdit,       8, 1, Qt::AlignLeft);

    gLayout->addWidget(agilityBoxLabel,     9, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(agilityEdit,         9, 1, Qt::AlignLeft);

    gLayout->addWidget(luckBoxLabel,       10, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(luckEdit,           10, 1, Qt::AlignLeft);

    gLayout->addWidget(isCurrentCheckLabel, 11, 0, Qt::AlignLeft|Qt::AlignTop);
    gLayout->addWidget(isCurrentCheck,      11, 1, Qt::AlignLeft);

    auto buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(okButton     );
    gLayout->addLayout(buttonLayout,        12, 0, Qt::AlignLeft|Qt::AlignTop);
    buttonLayout->addWidget(cancelButton);
    gLayout->addLayout(buttonLayout,        13, 0, Qt::AlignLeft|Qt::AlignTop);

    auto mainLayout = new QVBoxLayout;
    mainLayout->addLayout(gLayout);
    setLayout(mainLayout);

    connect(okButton,     &QAbstractButton::clicked, this, &QDialog::accept);
    connect(cancelButton, &QAbstractButton::clicked, this, &QDialog::reject);

    setWindowTitle(tr("Add a Persona"));
}

QString AddDialog::name()             const { return nameText->text();             }
QString AddDialog::arcana()           const { return arcanaText->text();           }
QString AddDialog::personality()      const { return personalityText->text();      }
QString AddDialog::trait()            const { return traitText->text();            }
QString AddDialog::defaultTrait()     const { return defaultTraitText->text();     }
uint    AddDialog::level()            const { return levelEdit->value();           }
uint    AddDialog::strength()         const { return strengthEdit->value();        }
uint    AddDialog::magic()            const { return magicEdit->value();           }
uint    AddDialog::endurance()        const { return enduranceEdit->value();       }
uint    AddDialog::agility()          const { return agilityEdit->value();         }
uint    AddDialog::luck()             const { return luckEdit->value();            }
Qt::CheckState AddDialog::isCurrent() const { return isCurrentCheck->checkState(); }

void AddDialog::editProfile(const QString &name, const QString &arcana, const QString &personality, const QString &trait,
           const QString &defaultTrait, const uint &level, const uint &strength, const uint &magic, const uint &endurance,
           const uint &agility,         const uint &luck,  const Qt::CheckState &isCurrent)
{
    nameText->setReadOnly(true);
    nameText->     setText(name);      arcanaText->       setText(arcana);       personalityText-> setText(personality);
    traitText->    setText(trait);     defaultTraitText-> setText(defaultTrait); levelEdit->       setValue(level);
    strengthEdit-> setValue(strength); magicEdit->        setValue(magic);       enduranceEdit->   setValue(endurance);
    agilityEdit->  setValue(agility);  luckEdit->         setValue(luck);        isCurrentCheck->  setCheckState(isCurrent);

}
