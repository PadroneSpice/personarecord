#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QCheckBox>
#include <QSpinBox>

QT_BEGIN_NAMESPACE
class QLabel;
class QPushButton;
class QTextEdit;
class QLineEdit;
QT_END_NAMESPACE

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    AddDialog(QWidget *parent = nullptr);

    // These are just functions to return the member data.
    QString name()         const;
    QString arcana()       const;
    QString personality()  const;
    QString trait()        const;
    QString defaultTrait() const;
    uint    level()        const;
    uint    strength()     const;
    uint    magic()        const;
    uint    endurance()    const;
    uint    agility()      const;
    uint    luck()         const;
    Qt::CheckState isCurrent()    const;
    void    editProfile(const QString &name, const QString &arcana, const QString &personality, const QString &trait,
                        const QString &defaultTrait, const uint &level, const uint &strength, const uint &magic, const uint &endurance,
                        const uint &agility, const uint &luck, const Qt::CheckState &isCurrent);

private:
    QLineEdit      *nameText;
    QLineEdit      *arcanaText;
    QLineEdit      *personalityText;
    QLineEdit      *traitText;
    QLineEdit      *defaultTraitText;
    QSpinBox       *levelEdit;
    QSpinBox       *strengthEdit;
    QSpinBox       *magicEdit;
    QSpinBox       *enduranceEdit;
    QSpinBox       *agilityEdit;
    QSpinBox       *luckEdit;
    QCheckBox      *isCurrentCheck;
};

#endif // ADDDIALOG_H
