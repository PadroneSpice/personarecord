#include "customsortfiltermodel.h"
#include <QDebug>

/* It's just passing the parent parameter to the base (original) class constructor. */
CustomSortFilterModel::CustomSortFilterModel(QObject *parent)
                     : QSortFilterProxyModel(parent)
{}


void CustomSortFilterModel::setArcana(QString givenArcana)
{
    arcana = givenArcana.toLower();
}

void CustomSortFilterModel::setFilterIsCurrent(QString isCurrentCategory)
{
    if (isCurrentCategory == "Current Roster") { filterIsCurrent = true;  }
    else                                       { filterIsCurrent = false; }
}

/* Call this to switch arcanaFiltering to true when needed. */
void CustomSortFilterModel::setArcanaFiltering(bool af)
{
    arcanaFiltering = af;
}

bool CustomSortFilterModel::lessThan(const QModelIndex &left,
                                     const QModelIndex &right) const
{
    QVariant leftData  = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);
    return false; //temporary
}

/* This is an override for the default filterAcceptsRow(). It's called automatically. */
bool CustomSortFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent); // Name
    QModelIndex index1 = sourceModel()->index(sourceRow, 1, sourceParent); // Arcana
    QModelIndex index2 = sourceModel()->index(sourceRow, 2, sourceParent); // Personality
    QModelIndex index3 = sourceModel()->index(sourceRow, 3, sourceParent); // Trait
    QModelIndex index4 = sourceModel()->index(sourceRow, 4, sourceParent); // Default Trait
    QModelIndex index5 = sourceModel()->index(sourceRow, 5, sourceParent); // Level
    QModelIndex index6 = sourceModel()->index(sourceRow, 6, sourceParent); // Current
    QModelIndex index11 = sourceModel()->index(sourceRow, 11, sourceParent); //

    //qInfo()<< "sourceModel()->data(index0): " << sourceModel()->data(index0);
    //qInfo()<< "sourceModel()->data(index1): " << sourceModel()->data(index1);
    //qInfo()<< "sourceModel()->data(index2): " << sourceModel()->data(index2);
    //qInfo()<< "sourceModel()->data(index4): " << sourceModel()->data(index4);
    //qInfo()<< "sourceModel()->data(index5): " << sourceModel()->data(index5);
    //qInfo()<< "sourceModel()->data(index6): " << sourceModel()->data(index6);

   // qInfo()<<"Arcana: " << arcana << " index1: " << index1.data().value<QString>().toLower();

    //qInfo()<<"Arcana filtering: " << arcanaFiltering;
    if (arcanaFiltering == false)
    {

        if ((index11.data().value<int>() == CHECKED   &&  filterIsCurrent)   // if in current team and this is the "current team" section
        || (index11.data().value<int>() == UNCHECKED  && !filterIsCurrent) ) // if not in current team and this is the "only registered" section
             { return true;  }
        else { return false; }
    }
    else
    {
        if (index1.data().value<QString>().toLower() == arcana)
             { return true;  }
        else { return false; }
    }
}

