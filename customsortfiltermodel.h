#ifndef CUSTOMSORTFILTERMODEL_H
#define CUSTOMSORTFILTERMODEL_H

#include <QSortFilterProxyModel>

class CustomSortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

const int UNCHECKED        = 0;
const int PARTIALLYCHECKED = 1;
const int CHECKED          = 2;

public:
    CustomSortFilterModel(QObject *parent = 0);

    void setFilterIsCurrent(QString isCurrentCategory);
    void setArcanaFiltering(bool af);
    void setArcana(QString givenArcana);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override; //overriding the base class definition

private:
    bool filterIsCurrent = false;   // 0: only registered;       1: current party
    bool arcanaFiltering = false;   // 0: filter not for arcana; 1: filter for arcana
    QString arcana;
};

#endif // CUSTOMSORTFILTERMODEL_H
