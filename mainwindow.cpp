#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAction>
#include <QFileDialog>
#include <QMenuBar>

MainWindow::MainWindow()
    : QMainWindow(), profileWidget(new ProfileWidget)
{
    setCentralWidget(profileWidget);
    createMenus();
    setWindowTitle(tr("Persona Book"));
}

/* Connections are declared here. */
void MainWindow::createMenus()
{
    // Declare the file menu.
    QMenu   *fileMenu = menuBar()->addMenu(tr("&File"));

    // Create the open action.
    QAction *openAct  = new QAction(tr("&Open..."), this);
    fileMenu->addAction(openAct);
    connect(openAct, &QAction::triggered, this, &MainWindow::openFile);

    // Create the save action.
    QAction *saveAct = new QAction(tr("&Save As..."), this);
    fileMenu->addAction(saveAct);
    connect(saveAct, &QAction::triggered, this, &MainWindow::saveFile);

    fileMenu->addSeparator(); // visual divider

    QAction *exitAct = new QAction(tr("E&xit"), this);
    fileMenu->addAction(exitAct);

    // Declare the tool menu.
    QMenu *toolMenu = menuBar()->addMenu(tr("&Tools"));

    // Create the add action.
    QAction *addAct = new QAction(tr("&Add Entry..."), this);
    toolMenu->addAction(addAct);
    connect(addAct, &QAction::triggered, profileWidget, &ProfileWidget::showAddEntryDialog);

    // Create the edit action.
    // Note that the syntax is a bit different here because editAct is a member of mainWindow.
    editAct = new QAction(tr("&Edit Entry..."), this);
    //editAct->setEnabled(false);
    toolMenu->addAction(editAct);
    connect(editAct, &QAction::triggered, profileWidget, &ProfileWidget::editEntry);
}

void MainWindow::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty())   { profileWidget->readFromFile(fileName); }
}

void MainWindow::saveFile()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    if (!fileName.isEmpty())   { profileWidget->writeToFile(fileName);  }
}

void MainWindow::updateActions(const QItemSelection &selection)
{
    QModelIndexList indexes = selection.indexes();

    if (!indexes.isEmpty())
    {
        removeAct->setEnabled(true);
        editAct->setEnabled(true);
    }
    else
    {
        removeAct->setEnabled(false);
        editAct->setEnabled(false);
    }
}
