#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "profilewidget.h"
#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

private slots:
    void updateActions(const QItemSelection &selection);
    void openFile();
    void saveFile();

private:
    void createMenus();

    ProfileWidget *profileWidget;
    QAction       *editAct;
    QAction       *removeAct;
};
#endif // MAINWINDOW_H
