#include "newprofiletab.h"
#include "adddialog.h"
#include <QTextStream>

#include <QtWidgets>

NewProfileTab::NewProfileTab(QWidget *parent)
    : QWidget(parent)
{
    auto descriptionLabel = new QLabel(tr("No Personas. Click to add."));
    auto addButton        = new QPushButton(tr("Add"));

    connect(addButton, &QAbstractButton::clicked, this, &NewProfileTab::addEntry);

    auto mainLayout       = new QVBoxLayout;
    mainLayout->addWidget(descriptionLabel);
    mainLayout->addWidget(addButton, 0, Qt::AlignCenter);

    setLayout(mainLayout);
}

void NewProfileTab::addEntry()
{
    AddDialog aDialog;
    if (aDialog.exec())
    {
        emit sendDetails(aDialog.name(),  aDialog.arcana(),   aDialog.personality(), aDialog.trait(),     aDialog.defaultTrait(),
                         aDialog.level(), aDialog.strength(), aDialog.magic(),       aDialog.endurance(), aDialog.agility(),
                         aDialog.luck(),  aDialog.isCurrent());
    }
}

