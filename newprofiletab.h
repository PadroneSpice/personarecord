#ifndef NEWPROFILETAB_H
#define NEWPROFILETAB_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QLabel;
class QPushButton;
class QVBoxLayout;
QT_END_NAMESPACE

class NewProfileTab : public QWidget
{
    Q_OBJECT

public:
    NewProfileTab(QWidget *parent = nullptr);

public slots:
    void addEntry();

signals:
    void sendDetails(const QString &name,         const QString &arcana, const QString &personality, const QString &trait,
                     const QString &defaultTrait, const uint &level,     const uint &strength,       const uint &magic,
                     const uint &endurance,       const uint &agility,   const uint &luck,           const Qt::CheckState &isCurrent);
};


#endif // NEWPROFILETAB_H
