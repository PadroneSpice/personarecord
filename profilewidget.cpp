#include "profilewidget.h"
#include "adddialog.h"
#include "newprofiletab.h"
#include "customsortfiltermodel.h"

#include <QtWidgets>
#include <QList>

ProfileWidget::ProfileWidget(QWidget *parent)
    : QTabWidget(parent),
      table        (new TableModel(this)),
      skillList    (new QList<QString>()),
      newProfileTab(new NewProfileTab(this))
{
    connect(newProfileTab, &NewProfileTab::sendDetails,
            this,          &ProfileWidget::addEntry);

    addTab(newProfileTab, tr("Persona Record"));

    setupTabs();
}


void ProfileWidget::showAddEntryDialog()
{
    AddDialog aDialog;

    if (aDialog.exec())
    {
        addEntry(aDialog.name(),         aDialog.arcana(),  aDialog.personality(), aDialog.trait(),
                 aDialog.defaultTrait(), aDialog.level(),   aDialog.strength(),    aDialog.magic(),
                 aDialog.endurance(),    aDialog.agility(), aDialog.luck(),        aDialog.isCurrent());
    }
}


/*
 * SLOT
 */
void ProfileWidget::addEntry(const QString &name, const QString &arcana, const QString &personality, const QString &trait,
                             const QString &defaultTrait, const uint &level,   const uint &strength, const uint &magic,
                             const uint    &endurance,    const uint &agility, const uint &luck,     const Qt::CheckState &isCurrent)
{

    // observation:
    // This is the order that the info is declared such that it is retrieved in the same order in the
    // custom filter.
    if (!table->getPersonas().contains({name, arcana, personality, trait, defaultTrait, level, strength, magic,
                                        endurance, agility, luck, isCurrent}))
     {
        table->insertRows(0, 1, QModelIndex());

        QModelIndex
        index = table->index(0, 0, QModelIndex());
        table->setData(index, name, Qt::EditRole);

        index = table->index(0, 1, QModelIndex());
        table->setData(index, arcana, Qt::EditRole);

        index = table->index(0, 2, QModelIndex());
        table->setData(index, personality, Qt::EditRole);

        index = table->index(0, 3, QModelIndex());
        table->setData(index, trait, Qt::EditRole);

        index = table->index(0, 4, QModelIndex());
        table->setData(index, defaultTrait, Qt::EditRole);

        index = table->index(0, 5, QModelIndex());
        table->setData(index, level,     Qt::EditRole);

        index = table->index(0, 6, QModelIndex());
        table->setData(index, strength,  Qt::EditRole);

        index = table->index(0, 7, QModelIndex());
        table->setData(index, magic,     Qt::EditRole);

        index = table->index(0, 8, QModelIndex());
        table->setData(index, endurance, Qt::EditRole);

        index = table->index(0, 9, QModelIndex());
        table->setData(index, agility,   Qt::EditRole);

        index = table->index(0, 10, QModelIndex());
        table->setData(index, luck,      Qt::EditRole);

        index = table->index(0, 11, QModelIndex());
        table->setData(index, isCurrent, Qt::EditRole);


        removeTab(indexOf(newProfileTab));
     }
    else
    {
        QMessageBox::information(this, tr("Duplicate Name"),
            tr("The name \"%1\" already exists.").arg(name));
    }
}


/*
 * SLOT
 * QTableView: Provides default view/model implementation of a table view
 * QSortFilterProxyModel: Provides support for sorting and filtering data passed between another model and a view.
 */
void ProfileWidget::editEntry()
{
    QTableView            *temp  = static_cast<QTableView*>(currentWidget());
    QSortFilterProxyModel *proxy = static_cast<QSortFilterProxyModel*>(temp->model());
    QItemSelectionModel   *selectionModel = temp->selectionModel();

    const QModelIndexList indexes = selectionModel->selectedRows();
    QString        name;
    QString        arcana;
    QString        personality;
    QString        trait;
    QString        defaultTrait;
    uint           level;
    uint           strength;
    uint           magic;
    uint           endurance;
    uint           agility;
    uint           luck;
    Qt::CheckState isCurrent;
    int row = -1;

    for (const QModelIndex &index : indexes)
    {
        // Name
        row = proxy->mapToSource(index).row();
        QModelIndex nameIndex       = table->index(row, 0, QModelIndex());
        QVariant    varName         = table->data(nameIndex, Qt::DisplayRole);
                    name            = varName.toString();
        // Arcana
        QModelIndex profileIndex    = table->index(row, 1, QModelIndex());
        QVariant    varArc          = table->data(profileIndex, Qt::DisplayRole);
                    arcana          = varArc.toString();
       // Personality
       QModelIndex personalityIndex = table->index(row, 2, QModelIndex());
       QVariant    varPer           = table->data(personalityIndex, Qt::DisplayRole);
                   personality      = varPer.toString();
       // Trait
       QModelIndex traitIndex       = table->index(row, 3, QModelIndex());
       QVariant    varTra           = table->data(traitIndex, Qt::DisplayRole);
                   trait            = varTra.toString();
       // Default Trait
       QModelIndex defaultTraitIndex = table->index(row, 4, QModelIndex());
       QVariant    varDef            = table->data(defaultTraitIndex, Qt::DisplayRole);
                   defaultTrait      = varDef.toString();
       // Level
       QModelIndex levelIndex        = table->index(row, 5, QModelIndex());
       QVariant    lelDef            = table->data(levelIndex, Qt::DisplayRole);
                   level             = lelDef.toUInt();
       // Strength
       QModelIndex strengthIndex     = table->index(row, 6, QModelIndex());
       QVariant    strDef            = table->data(strengthIndex, Qt::DisplayRole);
                   strength          = strDef.toUInt();
       // Magic
       QModelIndex magicIndex        = table->index(row, 7, QModelIndex());
       QVariant    magDef            = table->data(magicIndex, Qt::DisplayRole);
                   magic             = magDef.toUInt();
       // Endurance
       QModelIndex enduranceIndex    = table->index(row, 8, QModelIndex());
       QVariant    endDef            = table->data(enduranceIndex, Qt::DisplayRole);
                   endurance         = endDef.toUInt();
       // Agility
       QModelIndex agilityIndex      = table->index(row, 9, QModelIndex());
       QVariant    agiDef            = table->data(agilityIndex, Qt::DisplayRole);
                   agility           = agiDef.toUInt();
       // Luck
       QModelIndex luckIndex         = table->index(row, 10, QModelIndex());
       QVariant    lucDef            = table->data(luckIndex, Qt::DisplayRole);
                   luck              = lucDef.toUInt();

       // Is Current
       QModelIndex isCurrentIndex    = table->index(row, 11, QModelIndex());
       QVariant    iscDef            = table->data(isCurrentIndex, Qt::DisplayRole);

       // Not sure how else to set the checkbox, but this should work.
       if (iscDef == 0)   { isCurrent = Qt::Unchecked;        }
       if (iscDef == 1)   { isCurrent = Qt::PartiallyChecked; }
       if (iscDef == 2)   { isCurrent = Qt::Checked;          }
    }

    AddDialog aDialog;
    aDialog.setWindowTitle(tr("Edit a Persona"));
    aDialog.editProfile(name, arcana, personality, trait, defaultTrait, level, strength, magic,
                        endurance, agility, luck, isCurrent);

    if (aDialog.exec())
    {
        // Arcana
        const QString newArcana = aDialog.arcana();
        if (newArcana != arcana) {
            const QModelIndex index = table->index(row, 1, QModelIndex());
            table->setData(index, newArcana, Qt::EditRole);
        }
        // Personality
        const QString newPersonality = aDialog.personality();
        if (newPersonality != personality) {
            const QModelIndex index = table->index(row, 2, QModelIndex());
            table->setData(index, newPersonality, Qt::EditRole);
        }
        // Trait
        const QString newTrait = aDialog.personality();
        if (newTrait != trait) {
            const QModelIndex index = table->index(row, 3, QModelIndex());
            table->setData(index, newTrait, Qt::EditRole);
        }
        // Default Trait
        const QString newDefaultTrait = aDialog.personality();
        if (newDefaultTrait != defaultTrait) {
            const QModelIndex index = table->index(row, 4, QModelIndex());
                table->setData(index, newDefaultTrait, Qt::EditRole);
        }
        // Level
        const uint newLevel = aDialog.level();
        if (newLevel != level) {
            const QModelIndex index = table->index(row, 5, QModelIndex());
            table->setData(index, newLevel, Qt::EditRole);
        }
        // Strength
        const uint newStrength = aDialog.strength();
        if (newStrength != strength) {
            const QModelIndex index = table->index(row, 6, QModelIndex());
            table->setData(index, newStrength, Qt::EditRole);
        }
        // Magic
        const uint newMagic = aDialog.magic();
        if (newMagic != magic) {
            const QModelIndex index = table->index(row, 7, QModelIndex());
            table->setData(index, newMagic, Qt::EditRole);
        }
        // Endurance
        const uint newEndurance = aDialog.endurance();
        if (newEndurance != endurance) {
            const QModelIndex index = table->index(row, 8, QModelIndex());
            table->setData(index, newEndurance, Qt::EditRole);
        }
        // Agility
        const uint newAgility = aDialog.agility();
        if (newAgility != agility) {
            const QModelIndex index = table->index(row, 9, QModelIndex());
            table->setData(index, newAgility, Qt::EditRole);
        }
        // Luck
        const uint newLuck = aDialog.luck();
        if (newLuck != luck) {
            const QModelIndex index = table->index(row, 10, QModelIndex());
            table->setData(index, newLuck, Qt::EditRole);
        }
        // Is Current
        const Qt::CheckState newIsCurrent = aDialog.isCurrent();
        if (newIsCurrent != isCurrent) {
            const QModelIndex index = table->index(row, 11, QModelIndex());
            table->setData(index, newIsCurrent, Qt::EditRole);
        }
    }

}

void ProfileWidget::removeEntry()
{
    QTableView            *temp  = static_cast<QTableView*>(currentWidget());
    QSortFilterProxyModel *proxy = static_cast<QSortFilterProxyModel*>(temp->model());
    QItemSelectionModel   *selectionModel = temp->selectionModel();

    const QModelIndexList indexes = selectionModel->selectedRows();

    for (QModelIndex index : indexes) {
        int row = proxy->mapToSource(index).row();
        table->removeRows(row, 1, QModelIndex());
    }

    if (table->rowCount(QModelIndex()) == 0)
        insertTab(0, newProfileTab, tr("Persona Book"));
}


void ProfileWidget::setupTabs()
{
    const auto groups = {"Current Roster", "Only Registered"};
    const auto arcana = { "Fool",      "Magician",   "Priestess",  "Empress",
                          "Emperor",   "Hierophant", "Lovers",     "Chariot",
                          "Strength",  "Hermit",     "Fortune",    "Justice",
                          "Hanged",    "Death",      "Temperance", "Devil",
                          "Tower",     "Star",       "Moon",       "Sun",
                          "Judgement", "The World"}; // Toki yo tomare!

    for (const QString &str : groups)
    {
        auto proxyModel = new CustomSortFilterModel(this);

        // Link the proxy model to the table.
        proxyModel->setSourceModel(table);
        proxyModel->setFilterIsCurrent(str);
        proxyModel->setArcanaFiltering(false);

        proxyModel->setFilterKeyColumn(0);

        // Link the proxy model to the view.
        QTableView *tableView = new QTableView;
        tableView->setModel(proxyModel);

        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        //tableView->horizontalHeader()->setStretchLastSection(true);
        tableView->verticalHeader()->hide();
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView->setSortingEnabled(true);

        connect(tableView->selectionModel(), &QItemSelectionModel::selectionChanged,
                this, &ProfileWidget::selectionChanged);

        connect(this, &QTabWidget::currentChanged, this, [this, tableView](int tabIndex) {
            if (widget(tabIndex) == tableView)
                emit selectionChanged(tableView->selectionModel()->selection());
        });
        addTab(tableView, str);
    }

    for (const QString &str : arcana)
    {
        auto proxyModel = new CustomSortFilterModel(this);

        // Link the proxy model to the table.
        proxyModel->setSourceModel(table);
        proxyModel->setArcanaFiltering(true);
        proxyModel->setArcana(str);

        proxyModel->setFilterKeyColumn(0);

        // Link the proxy model to the view.
        QTableView *tableView = new QTableView;
        tableView->setModel(proxyModel);
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        //tableView->horizontalHeader()->setStretchLastSection(true);
        tableView->verticalHeader()->hide();
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView->setSortingEnabled(true);

        connect(tableView->selectionModel(), &QItemSelectionModel::selectionChanged,
                this, &ProfileWidget::selectionChanged);

        connect(this, &QTabWidget::currentChanged, this, [this, tableView](int tabIndex) {
            if (widget(tabIndex) == tableView)
                emit selectionChanged(tableView->selectionModel()->selection());
        });
        addTab(tableView, str);
    }
    //skillList->insert("HI");

}

void ProfileWidget::readFromFile(const QString &fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, tr("Unable to open the file."), file.errorString());
        return;
    }

    QVector<Persona> personas;
    QTextStream      in(&file);

   while (!in.atEnd())
   {
       QString     line   = in.readLine();
       QStringList fields = line.split(",");
       Persona p;
       if ((uint)fields.length() != dataTypeCount) { continue; }
       p.name     = fields[0];          p.arcana       = fields[1];          p.personality = fields[2];
       p.trait    = fields[3];          p.defaultTrait = fields[4];          p.level       = fields[5].toUInt();
       p.strength = fields[6].toUInt(); p.magic        = fields[7].toUInt(); p.endurance   = fields[8].toUInt();
       p.agility  = fields[9].toUInt(); p.luck         = fields[10].toUInt();
       if      (fields[11] == "2") { p.isCurrent = Qt::Checked;   }
       else if (fields[11] == "0") { p.isCurrent = Qt::Unchecked; }

       personas.insert(personas.size(), p);
   }

   file.close();

   if (personas.isEmpty())
   {
       QMessageBox::information(this, tr("No personas in the file."), tr("File contains no personas."));
   }
   else
   {
       for (const auto &persona: qAsConst(personas))
       {
           addEntry(persona.name,  persona.arcana,   persona.personality, persona.trait,     persona.defaultTrait,
                    persona.level, persona.strength, persona.magic,       persona.endurance, persona.agility,
                    persona.luck,  persona.isCurrent);
       }
   }
}

void ProfileWidget::writeToFile(const QString &fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"), file.errorString());
        return;
    }

    QTextStream out(&file);
    QVector<Persona> personas = table->getPersonas();
    for (auto p : personas)
    {
        out<<p.name<<","<<p.arcana<<","<<p.personality<<","<<p.trait<<","<<p.defaultTrait<<","
           <<p.level<<","<<p.strength<<","<<p.magic<<","<<p.endurance<<","<<p.agility<<","<<p.luck<<","
           <<p.isCurrent<< "\n";
    }

    file.close();
}
