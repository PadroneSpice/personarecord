#ifndef PROFILEWIDGET_H
#define PROFILEWIDGET_H

#include "newprofiletab.h"
#include "tablemodel.h"

#include <QItemSelection>
#include <QTabWidget>
#include <QListView>

QT_BEGIN_NAMESPACE
class QSortFilterProxyModel;  // used for data filtering
class QItemSelectionModel;    // keeps track of view's selected items
QT_END_NAMESPACE

class ProfileWidget : public QTabWidget
{
    Q_OBJECT

public:
    ProfileWidget(QWidget *parent = nullptr);
    void readFromFile(const QString &fileName);
    void writeToFile (const QString &fileName);

public slots:
    void showAddEntryDialog();
    void addEntry(const QString &name,         const QString &arcana,  const QString &personality,  const QString &trait,
                  const QString &defaultTrait, const uint    &level,   const uint &strength,        const uint    &magic,
                  const uint    &endurance,    const uint    &agility, const uint &luck,
                  const Qt::CheckState &isCurrent);
    void editEntry();
    void removeEntry();

signals:
    void selectionChanged (const QItemSelection &selected);

private:
    void setupTabs();

    TableModel     *table;
    QList<QString> *skillList;
    NewProfileTab  *newProfileTab;
};

#endif // PROFILEWIDGET_H
