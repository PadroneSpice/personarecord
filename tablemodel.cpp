#include "tablemodel.h"
#include <QTextStream>
#include <iostream>

#include <QDebug>

TableModel::TableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

TableModel::TableModel(const QVector<Persona> &personas, QObject *parent)
    : QAbstractTableModel(parent),
      personas(personas)
{
}

/* Returns the number of rows. */
int TableModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : personas.size();
}

/* Returns the number of columns. */
int TableModel::columnCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : dataTypeCount;
}


QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= personas.size() || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole) {
        const auto &persona = personas.at(index.row());

        switch (index.column()) {
            case 0:
                return persona.name;
            case 1:
                return persona.arcana;
            case 2:
                return persona.personality;
            case 3:
                return persona.trait;
            case 4:
                return persona.defaultTrait;
            case 5:
                return persona.level;
            case 6:
                return persona.strength;
            case 7:
                return persona.magic;
            case 8:
                return persona.endurance;
            case 9:
                return persona.agility;
            case 10:
                return persona.luck;
            case 11:
                return persona.isCurrent;
            default:
                break;
        }
    }
    return QVariant();
}

/**
 * These switch cases are for the data category labels.
 * A QVariant is essentially a union for Qt data types, which is necessary because
 * native C++ unions are forbidden from including types with non-default constructors
 * and destructors.
 *
 * tr() is a wrapper function that makes user-visible text translatable.
 *
 * The plain QVariant() is just a construction of an invalid variant,
 * so it's basically for returning no information.
 */
QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Arcana");
            case 2:
                return tr("Personality");
            case 3:
                return tr("Trait");
            case 4:
                return tr("Default Trait");
            case 5:
                return tr("Level");
            case 6:
                return tr("Strength");
            case 7:
                return tr("Magic");
            case 8:
                return tr("Endurance");
            case 9:
                return tr("Agility");
            case 10:
                return tr("Luck");
            case 11:
                return tr("Current Team?");
            default:
                break;
        }
    }
    return QVariant();
}

bool TableModel::insertRows(int position, int rows, const QModelIndex &index)
{
    Q_UNUSED(index);
    beginInsertRows(QModelIndex(), position, position + rows - 1);

    for (int row = 0; row < rows; ++row)
        personas.insert(position, { QString(), QString() , QString(), QString(), QString(), uint(),
                                    uint(), uint(), uint(), uint(), uint(), Qt::CheckState()});

    endInsertRows();
    return true;
}

bool TableModel::removeRows(int position, int rows, const QModelIndex &index)
{
    Q_UNUSED(index);
    beginRemoveRows(QModelIndex(), position, position + rows - 1);

    for (int row = 0; row < rows; ++row)
        personas.removeAt(position);

    endRemoveRows();
    return true;
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    if (index.isValid() && role == Qt::EditRole) {
        const int row     = index.row();
        auto      persona = personas.value(row);

        callCounter++;
        switch (index.column()) { // index.column() == the column the index refers to

            case 0:
                persona.name         = value.toString();
                break;
            case 1:
                persona.arcana       = value.toString();
                break;
            case 2:
                persona.personality  = value.toString();
                break;
            case 3:
                persona.trait        = value.toString();
                break;
            case 4:
                persona.defaultTrait = value.toString();
                break;
            case 5:
                persona.level        = value.toUInt();
                break;
            case 6:
                persona.strength     = value.toUInt();
                break;
            case 7:
                persona.magic        = value.toUInt();
                break;
            case 8:
                persona.endurance    = value.toUInt();
                break;
            case 9:
                persona.agility      = value.toUInt();
                break;
            case 10:
                persona.luck         = value.toUInt();
                break;
            case 11:
                if (value == 0) { persona.isCurrent = Qt::Unchecked;        }
                if (value == 1) { persona.isCurrent = Qt::PartiallyChecked; } // This shouldn't actually happen.
                if (value == 2) { persona.isCurrent = Qt::Checked;          }
                break;
            default:
                return false;
        }
        personas.replace(row, persona);
        emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});  // EVENT: Data has changed!

        return true;
    }
    return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

const QVector<Persona> &TableModel::getPersonas() const
{
    return personas;
}


