#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QAbstractTableModel>
#include <QVector>

const uint dataTypeCount = 12; // Number of pieces of Persona info

struct Persona
{
    QString name;
    QString arcana;
    QString personality;
    QString trait;
    QString defaultTrait;
    uint    level;
    uint    strength;
    uint    magic;
    uint    endurance;
    uint    agility;
    uint    luck;
    Qt::CheckState isCurrent;

    //QString skills[8];
    //QString elements[10];

    bool operator==(const Persona &other) const
    {
        return name == other.name;
    }
};

inline QDataStream &operator<<(QDataStream &stream, const Persona &p)
{
    return stream << p.name;
}

inline QDataStream &operator>>(QDataStream &stream, Persona &p)
{
    return stream >> p.name;
}

class TableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    TableModel(QObject *parent = nullptr);
    TableModel(const QVector<Persona> &personas, QObject *parent = nullptr);
    int      rowCount(const QModelIndex &parent) const override;
    int      columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool     setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    bool     insertRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
    bool     removeRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
    const QVector<Persona> &getPersonas() const;

private:
    QVector<Persona> personas;
    int callCounter = 0;
};

#endif // TABLEMODEL_H
